#pragma once
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <regex>
#include <iostream>
#include <cmath>
#include <numeric>


/**
* @brief Data storing class. 
* Used for all data operations, such as reading from file, shuffling, splitting etc.
*/
class dataManagement
{
	/**
	* @brief Constant denoting percentage of data assigned to train set
	*/
	const double TRAIN_SET_PERCENT = 0.75;
	
	/**
	* @brief Constant denoting percentage of data assigned to test set
	*/
	const double TEST_SET_PERCENT = 0.2;
	
	/**
	* @brief Constant denoting percentage of data assigned to validation set
	*/
	const double VALIDATION_SET_PERCENT = 0.05;

public:
	/**
	* Vector of double vectors storing whole raw data.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> loadData;

	/**
	* Vector of double vectors storing data used for testing
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> testData;

	/**
	* Vector of double vectors storing training data 
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> trainData;

	/**
	* Vector of double vectors storing data not invloved in training process but in validation of a created model.
	* Contains both features and a label, each in seperate sub vector.
	*/	std::vector<std::vector<double>> validationData;

	/**
	* Default constructor
	*/
	dataManagement() = default;

	/**
	* Default destructor
	*/
	~dataManagement() = default;

	/**
	* Method that scales dataset using z score standarization or min/max standarization
	* @param useZScore Boolean that if set to true uses z score standarization, otherwise uses min/max
	*/
	void dataScaling(bool useZScore);

	/**
	* A wrapper method that calls all optimisation functions
	* @param useZScore Boolean that if set to true uses z score standarization, otherwise uses min/max
	* @see dataScaling()
	* @see shuffleData()
	* @see splitData()
	*/
	void optimiseData(bool useZScore);
	
	/**
	* Reads data from a bitmap file given by a user
	* @param filename Char pointer to name of the file given by the user
	* @see readFromCsvOrTxt()
	*/
	void readFromBmp(char* filename);

	/**
	* Reads data from either a csv or txt file given by a user
	* @param filename Name of the file given by the user
	* @param delimiter Character used as a delimiter between values in the file
	* @param numberOfColumns Amount of columns of data existing in the file
	* @see readFromBmp()
	*/
	void readFromCsvOrTxt(std::string fileName, char delimiter, int numberOfColumns);
	
	/**
	* Randomizes the ordering of rows of loaded data
	*/
	void shuffleData();

	/**
	* Separates loaded data into three sub containers.	
	*/
	void splitData();
	
	

};