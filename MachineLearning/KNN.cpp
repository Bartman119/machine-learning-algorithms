#include "KNN.h"
#include <cmath>
#include <limits>
#include <map>
#include <stdint.h>
#include <iostream>

knn::knn(int val)
{
	k = val;
}

knn::knn()
{
}

double knn::calculateDistance(std::vector<std::vector<double>> queryPoint, std::vector<std::vector<double>> input, int queryIndex, int inputIndex)
{
	double distance = 0;
	for (int i = 0; i < queryPoint.size() - 1; i++)
	{
		distance += pow(queryPoint[i].at(queryIndex) - input[i].at(inputIndex), 2);
	}
	distance = sqrt(distance);
	return distance;
}

void knn::fit(dataManagement & loadedData)
{
	//saving points for comparisons
	this->setTrainingData(loadedData.trainData);
	this->setTestData(loadedData.testData);
	this->setValidationData(loadedData.validationData);
}

int knn::predict()
{
	std::map<double, int> classFreq;
	for (int i = 0; i < neighbours[0].size(); i++)
	{
		//if class label is not present here
		if (classFreq.find(neighbours[neighbours.size() - 1].at(i)) == classFreq.end())
		{
			classFreq[neighbours[neighbours.size() - 1].at(i)] = 1;
		}
		else
		{
			//increment when value has already occured in a map
			classFreq[neighbours[neighbours.size() - 1].at(i)]++;
		}
	}

	int best = 0;
	int max = 0;
	for (auto kv : classFreq)
	{
		if (kv.second > max)
		{
			max = kv.second;
			best = kv.first;
		}
	}
	neighbours.clear();
	return best;
}

void knn::runAlgorithm(dataManagement & loadedData, std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	fit(loadedData);
	this->validatePerformance(textBoxVector);
	this->testPerformance(textBoxVector);
}

void knn::setK(tgui::Slider::Ptr slider)
{
	this->k = slider->getValue();
}

void knn::setTestData(std::vector<std::vector<double>> data)
{
	this->modelTestData = data;
}

void knn::setTrainingData(std::vector<std::vector<double>> data)
{
	this->modelTrainingData = data;
}

void knn::setValidationData(std::vector<std::vector<double>> data)
{
	this->modelValidationData = data;
}

double knn::testPerformance(std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	double currentPerformance = 0;
	int count = 0;

	for (int i = 0; i < modelTestData[0].size(); i++)
	{
		train(modelTestData, i);
		int prediction = predict();
		printf("%d -> %d \n", prediction, (int)modelTestData[modelTestData.size() - 1].at(i));
		textBoxVector.at(KNNTestResults)->addText(std::to_string(prediction) + " -> " + std::to_string((int)modelTestData[modelTestData.size() - 1].at(i)) + "\n");
		if (prediction == modelTestData[modelTestData.size() - 1].at(i))
		{
			count++;
		}
	}
	currentPerformance = ((double)count*100.0) / ((double)modelTestData[0].size());
	printf("Tested Performance = %.3f %%\n", currentPerformance);
	textBoxVector.at(KNNTestResults)->addText("Tested Performance = " + std::to_string(currentPerformance) + "%\n");
	return currentPerformance;
}

void knn::train(std::vector<std::vector<double>> queryPoint, int queryIndex)
{
	double min = std::numeric_limits<double>::max();
	double previous_min = min;
	int index = 0;
	for (int i = 0; i < k; i++) 
	{
		if (i == 0)
		{
			// for each point of data calculate euclidean distance between given point and each every point in training data
			for (int j = 0; j < modelTrainingData[0].size(); j++)
			{
				double distance = calculateDistance(queryPoint, modelTrainingData, queryIndex, j);//need a queryPoint index!
				if (distance < min)
				{
					min = distance;
					index = j;
				}
			}
			for (int q = 0; q < modelTrainingData.size(); q++)
			{
				std::vector<double> newNeighbour;
				newNeighbour.push_back((modelTrainingData[q].at(index)));
				neighbours.push_back(newNeighbour);
			}
			previous_min = min;
			min = std::numeric_limits<double>::max();
		}
		else
		{
			//find k closest neighbours to the point
			for (int j = 0; j < modelTrainingData[0].size(); j++)
			{
				double distance = calculateDistance(queryPoint, modelTrainingData, queryIndex, j);//need a queryPoint index!
				if (distance > previous_min && distance < min)
				{
					min = distance;
					index = j;
				}
			}
			for (int q = 0; q < modelTrainingData.size(); q++)
			{
				std::vector<double> newNeighbour;
				newNeighbour.push_back((modelTrainingData[q].at(index)));
				neighbours.push_back(newNeighbour);
			}
			previous_min = min;
			min = std::numeric_limits<double>::max();
		}
	}
}

double knn::validatePerformance(std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	double currentPerformance = 0;
	int count = 0;
	int data_index = 0;

	for (int i = 0; i < modelValidationData[0].size(); i++)
	{
		train(modelValidationData, i);
		int prediction = predict();
		printf("%d -> %d \n", prediction, (int)modelValidationData[modelValidationData.size() - 1].at(i));
		textBoxVector.at(KNNTrainingInfo)->addText(std::to_string(prediction) + " -> " + std::to_string((int)modelValidationData[modelValidationData.size() - 1].at(i)) + "\n");
		if (prediction == modelValidationData[modelValidationData.size() - 1].at(i))
		{
			count++;
		}
		data_index++;
		printf("Current Performance = %.3f %%\n", ((double)count*100.0)/((double)data_index));
		textBoxVector.at(KNNTrainingInfo)->addText("Current Performance = " + std::to_string((((double)count*100.0) / ((double)data_index))) + "%\n");
	}
	currentPerformance = ((double)count*100.0) / ((double)modelValidationData[0].size());
	printf("Validation Performance for K = %d: %.3f %%\n",k, currentPerformance);
	textBoxVector.at(KNNTrainingInfo)->addText("Validation Performance for K = " + std::to_string(k) + ": " + std::to_string(currentPerformance) + "%\n");
	return currentPerformance;
}

