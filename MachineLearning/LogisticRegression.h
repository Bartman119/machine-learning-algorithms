#pragma once
#ifndef LOGISTIC_REGRESSION_H
#define LOGISTIC_REGRESSION_H
#include <iostream>
#include <vector>
#include <cmath>
#include "AlgorithmStrategy.h"
#include "dataManagement.h"

/**
* @brief Logistic Regression algorithm class.
* Derived from AlgorithmStrategy class.
* Used for utilization of Logistic Regression algorithm in the program. Part of a strategy pattern.
* @see AlgorithmStrategy
*/
class LogisticRegression: public AlgorithmStrategy
{
public:
	/**
	* Vector of double vectors storing data used for testing of a model.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelTestData;
	
	/**
	* Vector of double vectors storing training data of a model.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelTrainingData;

	/**
	* Vector of double vectors storing data used for model validation.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelValidationData;

	/**
	* Vector of doubles storing predicted values for each data point.
	*/
	std::vector<double> predictedValues;

	/**
	* Parameter used as bias in logistic regression.
	*/
	double b0 = 0.0;
	
	/**
	* Parameter used as first feature coefficient in logistic regression.
	*/
	double b1 = 0.0;
	
	/**
	* Parameter used as second feature coefficient in logistic regression.
	*/
	double b2 = 0.0;


	/**
	* Hyperparameter determining threshold value after which the calculations of parameters will stop.
	*/
	double convergeThreshold = 0.001;
	
	/**
	* Hyperparameter used to determine the step of a function.
	*/
	double learningRate = 0.01;

	/**
	* Hyperparameter used to determine threshold after which proper value is assigned as predicted label value.
	*/
	double probabilityThreshold = 0.5;

	/**
	* Default constructor.
	*/
	LogisticRegression();
	
	/**
	* Default destructor.
	*/
	~LogisticRegression() = default;
	
	/**
	* Calculates current value of b0 parameter.
	* @param yPoints Vector of doubles of y coordinates.
	* @return Double value of b0 parameter.
	*/
	double calculateB0Derivative(std::vector<double> yPoints);
	
	/**
	* Calculates current value of b1 parameter.
	* @param xPoints Vector of doubles of x coordinates.
	* @param yPoints Vector of doubles of y coordinates.
	* @return Double value of b1 parameter.
	*/
	double calculateB1Derivative(std::vector<double> xPoints, std::vector<double> yPoints);
	
	/**
	* Calculates current value of b2 parameter.
	* @param xPoints Vector of doubles of x coordinates.
	* @param yPoints Vector of doubles of y coordinates.
	* @return Double value of b2 parameter.
	*/
	double calculateB2Derivative(std::vector<double> xPoints, std::vector<double> yPoints);
	
	/**
	* Calculates cost function of current iteration.
	* @param yPoints Vector of doubles of y coordinates.
	* @return Double value of b1 parameter.
	*/
	double calculateCostFunction(std::vector<double> yPoints);

	/**
	* Overridden method that trains the model by modifying b parameters over a training dataset.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	*/
	void fit(std::vector<tgui::TextBox::Ptr> textBoxVector);

	/**
	* b0 parameter getter.
	* @return b0 parameter.
	*/
	double getB0();
	
	/**
	* b1 parameter getter.
	* @return b1 parameter.
	*/
	double getB1();
	
	/**
	* b2 parameter getter.
	* @return b2 parameter.
	*/
	double getB2();
	
	/**
	* convergeThreshold hyperparameter getter.
	* @return convergeThreshold hyperparameter.
	*/
	double getConvergeThreshold();
	
	/**
	* learningRate hyperparameter getter.
	* @return learningRate hyperparameter.
	*/
	double getLearningRate();
	
	/**
	* probabilityThreshold hyperparameter getter.
	* @return probabilityThreshold hyperparameter.
	*/
	double getProbabilityThreshold();
	
	/**
	* Predicts the value of each data point using calculated coefficients.
	* @param yPoints Vector of doubles of y coordinates.
	* @param xPoints Vector of doubles of x coordinates.
	*/
	void predict(std::vector<double> yPoints, std::vector<double> xPoints);

	/**
	* Overriden wrapper method calling all LogisticRegression methods to execute an algorithm.
	* @param loadedData dataManagement class pointer containing dataset for an algorithm to operate on.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	* @see setTrainingData()
	* @see setTestData()
	* @see setValidationData()
	* @see fit()
	* @see testPerformance()
	*/
	void runAlgorithm(dataManagement & loadedData, std::vector<tgui::TextBox::Ptr> textBoxVector);
	
	/**
	* Setter modifying b0 parameter.
	* @param b0 New b0 value.
	*/
	void setB0(double b0);

	/**
	* Setter modifying b1 parameter.
	* @param b1 New b1 value.
	*/
	void setB1(double b1);

	/**
	* Setter modifying b2 parameter.
	* @param b2 New b2 value.
	*/
	void setB2(double b2);

	/**
	* Setter modifying convergeThreshold hyperparameter..
	* @param slider Tgui slider from which current value of a hyperparameter is taken.
	*/
	void setConvergeThreshold(tgui::Slider::Ptr slider);

	/**
	* Setter modifying learningRate hyperparameter..
	* @param slider Tgui slider from which current value of a hyperparameter is taken.
	*/
	void setLearningRate(tgui::Slider::Ptr slider);

	/**
	* Setter modifying probabilityThreshold hyperparameter..
	* @param slider Tgui slider from which current value of a hyperparameter is taken.
	*/
	void setProbabilityThreshold(tgui::Slider::Ptr slider);

	/**
	* Training data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setTestData(std::vector<std::vector<double>> data);

	/**
	* Test data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setTrainingData(std::vector<std::vector<double>> data);
	
	/**
	* Validation data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setValidationData(std::vector<std::vector<double>> data);

	/**
	* Method predicting labels of test data points using logistic regression formula.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	*/
	void testPerformance(std::vector<tgui::TextBox::Ptr> textBoxVector);
};

#endif