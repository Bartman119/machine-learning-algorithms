#pragma once
#ifndef KNN_H
#define KNN_H

#include <vector> 
#include "dataManagement.h"
#include "AlgorithmStrategy.h"

/**
* @brief KNN algorithm class.
* Derived from AlgorithmStrategy class.
* Used for utilization of KNN algorithm in the program. Part of a strategy pattern.
* @see AlgorithmStrategy
*/
class knn: public AlgorithmStrategy
{
	/**
	* Private integer k storing amount of neighbours to be used in algorithm. 
	*/
	int k;

	/**
	* Vector of double vectors storing data used for testing of a model.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelTestData;

	/**
	* Vector of double vectors storing training data of a model.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelTrainingData;

	/**
	* Vector of double vectors storing data used for model validation.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelValidationData;

	/**
	* Vector of double vectors that contains all information of closest neighbouring points to an examined point of data.
	*/
	std::vector<std::vector<double>> neighbours;
public:
	/**
	* Constructor taking integer value and assigning it to k variable.
	*/
	knn(int);

	/**
	* Default class constructor.
	*/
	knn();
	
	/**
	* Default class destructor.
	*/
	~knn() = default;

	/**
	* Calculates Euclidean distance between two points of data.
	* @param queryPoint Vector of double vectors dataset, for which points we look for neighbours.
	* @param input Vector of double vectors dataset used for comparisons.
	* @param queryIndex Integer index used to access certain point of queryPoint data.
	* @param inputIndex Integer index used to access certain point of input data.
	* @return Double value of distance between the data points.
	*/
	double calculateDistance(std::vector<std::vector<double>> queryPoint, std::vector<std::vector<double>> input, int queryIndex, int inputIndex);
	
	/**
	* Overridden method that trains the model by assigning point to training set.
	* @param loadedData dataManagement class pointer containing dataset for an algorithm to operate on.
	*/
	void fit(dataManagement & loadedData);
	
	/**
	* Method predicting the class of a data point 
	*/
	int predict();
	
	/**
	* Overriden wrapper method calling all KNN methods to execute an algorithm.
	* @param loadedData dataManagement class pointer containing dataset for an algorithm to operate on.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	* @see fit()
	* @see validatePerformance()
	* @see testPerformance()
	*/
	void runAlgorithm(dataManagement & loadedData, std::vector<tgui::TextBox::Ptr> textBoxVector);
	
	/**
	* k neighbours variable setter.
	* @param slider Tgui slider from which current value of a hyperparameter is taken. 
	*/
	void setK(tgui::Slider::Ptr slider);

	/**
	* Test data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setTestData(std::vector<std::vector<double>> data);
	
	/**
	* Training data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setTrainingData(std::vector<std::vector<double>> data);
	
	/**
	* Validation data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setValidationData(std::vector<std::vector<double>> data);
	
	/**
	* Method testing how well model works after training.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	* @return Double percentage value of model's correctness.
	*/
	double testPerformance(std::vector<tgui::TextBox::Ptr> textBoxVector);
	
	/**
	* Finds closest k neighbours to query points.
	* @param queryPoint Vector of double vectors dataset, for which points we look for neighbours.
	* @param queryIndex Integer index used to access certain point of queryPoint data.
	*/
	void train(std::vector<std::vector<double>> queryPoint, int queryIndex);
	
	/**
	* Method finding the best model.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	* @return Double percentage value of model's correctness.
	*/
	double validatePerformance(std::vector<tgui::TextBox::Ptr> textBoxVector);
	

};

#endif KNN_H