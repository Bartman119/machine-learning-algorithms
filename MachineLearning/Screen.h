#pragma once
#ifndef SCREEN_H
#define SCREEN_H
#include <TGUI/TGUI.hpp>
#include "dataManagement.h"
#include <exception>
#include "AlgorithmStrategy.h"
#include "LinearRegression.h"
#include "LogisticRegression.h"
#include "KNN.h"

/**
* @brief Screen class.
* Contains all tools to display a given screen to a window.
*/
class Screen
{
public:
	/**
	* Instance of dataManagement class.
	* Used to pass data to chosen algorithm's containers.
	*/
	dataManagement _loadedData;

	/**
	* Instance of knn class that is called when such algorithm is chosen by a user.
	* @see knn
	*/
	knn _knnInstance;

	/**
	* Instance of LinearRegression class that is called when such algorithm is chosen by a user.
	* @see LinearRegression
	*/
	LinearRegression _linRegInstance;

	/**
	* Instance of LogisticRegression class that is called when such algorithm is chosen by a user.
	* @see LogisticRegression
	*/
	LogisticRegression _logRegInstance;

	/**
	* textBoxNumber enum structure.
	* Contains values used to determine which screen should be displayed.
	*/
	enum screenNumber 
	{
		startingScreen = 0, /**< Value corresponding to displaying a starting screen*/
		mainScreen, /**< Value corresponding to displaying a main menu screen*/
		linReg, /**< Value corresponding to displaying a linear regression screen*/
		logReg, /**< Value corresponding to displaying a logistic regression screen*/
		KNN /**< Value corresponding to displaying a knn screen*/
	};

	/**
	* Vector of tgui Panels that store all information about each screen.
	*/
	std::vector<tgui::Panel::Ptr> screenVector;

	/**
	* Vector of tgui textBoxes used to output information during training and testing to gui. 
	*/
	std::vector<tgui::TextBox::Ptr> textBoxVector;


	/**
	* Screen constructor that takes loadedData class and assigns to its own attribute.
	* @param loadedData dataManagement class holding all data read from a file.
	*/
	Screen(dataManagement loadedData);

	/**
	* Selects screen to display based on the input index.
	* @param screenPosition Index at which desired screen is located. 
	*/
	void chooseScreenToDisplay(int screenPosition);
	
	/**
	* Creates all tgui elements that knn screen consists of. 
	*/
	void createKNNScreen();
	
	/**
	* Creates all tgui elements that linear regression screen consists of.
	*/
	void createLinRegScreen();

	/**
	* Creates all tgui elements that logistic regression screen consists of.
	*/
	void createLogRegScreen();

	/**
	* Creates all tgui elements that starting screen consists of.
	*/
	void createStartingScreen();

	/**
	* Creates all tgui elements that main menu screen consists of.
	*/
	void createMainScreen();

	/**
	* knn class instance getter.
	* @return knn class instance.
	*/
	knn getKNNInstance() { return this->_knnInstance; };
	
	/**
	* logisticRegression class instance getter.
	* @return logisticRegression class instance.
	*/
	LogisticRegression getLogRegInstance() { return this->_logRegInstance; };

	/**
	* linearRegression class instance getter.
	* @return linearRegression class instance.
	*/
	LinearRegression getLinRegInstance() { return this->_linRegInstance; };
	
	/**
	* Loop responsible for refreshing gui and catching/handling program's events.
	*/
	void guiLoop();

	/**
	* Takes file name from tgui Text Box and loads its data.
	* @param inputFile Tgui textBox used to input or output data.
	*/
	void loadCorrectData(tgui::EditBox::Ptr inputFile);

	/**
	* Calls for selected strategy's runAlgorithm method.
	* @param as Class derived from AlgorithmStrategy class .
	* @param loadedData dataManagement class that holds all data from a file.
	* @see AlgorithmStrategy
	*/
	void runAlgorithm(AlgorithmStrategy & as, dataManagement & loadedData);
	
	/**
	* Updates tgui Label based on tgui slider value.
	* @param dataSlider Tgui slider used to update the label value.
	* @param outputLabel Tgui label whose value is updated.
	*/
	void updateSliderDisplay(tgui::Slider::Ptr dataSlider, tgui::Label::Ptr outputLabel);

protected:
	/**
	* Holds tgui theme of the whole gui.
	*/
	tgui::Theme basicTheme;

private:
	/**
	* SFML library class to render a window.
	*/
	sf::RenderWindow window;

	/**
	* Tgui gui class for all gui operations.
	*/
	tgui::Gui gui;
};
#endif 