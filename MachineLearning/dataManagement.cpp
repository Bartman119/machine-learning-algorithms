#include "dataManagement.h"
void dataManagement::dataScaling(bool useZScore)
{
	if (useZScore)
	{
		//standarize features only, leave label unchanged
		for (int i = 0; i < this->loadData.size() - 1; i++)
		{
			double sum = std::accumulate(this->loadData[i].begin(), this->loadData[i].end(), 0.0);
			double mean = sum / this->loadData[i].size();

			double sq_sum = std::inner_product(this->loadData[i].begin(), this->loadData[i].end(), this->loadData[i].begin(), 0.0);
			double stdev = std::sqrt(sq_sum / this->loadData[i].size() - mean * mean);

			for (int j = 0; j < this->loadData[i].size(); j++)
			{
				double zScore = (this->loadData[i].at(j) - mean) / stdev;
				this->loadData[i].at(j) = zScore;
			}
		}
	}
	else
	{
		for (int i = 0; i < this->loadData.size() - 1; i++)
		{
			double max = *std::max_element(this->loadData[i].begin(), this->loadData[i].end());
			double min = *std::min_element(this->loadData[i].begin(), this->loadData[i].end());

			for (int j = 0; j < this->loadData[i].size(); j++)
			{
				double minMaxStandarization = (this->loadData[i].at(j) - min) / (max - min);
				this->loadData[i].at(j) = minMaxStandarization;
			}
		}

	}
}

void dataManagement::optimiseData(bool useZScore)
{
	this->dataScaling(useZScore);
	this->shuffleData();
	this->splitData();
}

void dataManagement::readFromBmp(char * filename)
{
	if (this->loadData.size() > 0)
	{
			this->loadData.clear();
			this->trainData.clear();
			this->testData.clear();
			this->validationData.clear();
	}
	//create 3 columns, two for coordinates and one for group representation
	for (int i = 0; i < 3; i++)
	{
		std::vector<double> v1;
		this->loadData.push_back(v1);
	}

	int i;
	FILE* f = fopen(filename, "rb");
	unsigned char info[54];

	// read the 54-byte header
	fread(info, sizeof(unsigned char), 54, f);

	// extract image height and width from header
	int width = *(int*)&info[18];
	int height = *(int*)&info[22];

	// allocate 3 bytes per pixel
	int size = 3 * width * height;
	unsigned char* data = new unsigned char[size];

	// read the rest of the data at once
	fread(data, sizeof(unsigned char), size, f);
	fclose(f);

	int x = 0;
	int y = 127;
	for (i = 0; i < size; i += 3)
	{
		if (x == width)
		{
			x = 0;
			y--;
		}
		std::cout << (int)data[i] << " " << (int)data[i + 1] << " " << (int)data[i + 2] << " " << std::endl;
		//found black pixel
		if ((int)data[i] == 0 && (int)data[i + 1] == 0 && (int)data[i + 2] == 0)
		{
			this->loadData[0].push_back(x);
			this->loadData[1].push_back(y);
			this->loadData[2].push_back(0);
		}
		else if ((int)data[i] == 255 && (int)data[i + 1] == 255 && (int)data[i + 2] == 255)
		{
			this->loadData[0].push_back(x);
			this->loadData[1].push_back(y);
			this->loadData[2].push_back(1);
		}
		x++;
	}
	delete data;
}



void dataManagement::readFromCsvOrTxt(std::string fileName/*, std::vector<std::vector<int>> * loadData*/, char delimiter, int numberOfColumns)
{
	if (this->loadData.size() > 0)
	{
		this->loadData.clear();
		this->trainData.clear();
		this->testData.clear();
		this->validationData.clear();
	}
	//create as many vectors storing columns as determined by numberOfColumns parameter
	for (int i = 0; i < numberOfColumns; i++) 
	{
		std::vector<double> v1;
		this->loadData.push_back(v1);
	}

	std::fstream fileIn;
	fileIn.open(fileName, std::ios::in);

	std::string cell, row;
	//regex for checking data format
	std::regex myReg{ R"((\d+[\.,]?(\d+)?;)+(\d+[\.,]?(\d+)?))" };
	
	//read data row by row and add values to correct vectors
	while (fileIn >> row) 
	{
		int iteration = 0;
		//checks whether line is read without any unnecessary characters
		if (!std::regex_match(row, myReg))
		{
			std::smatch match;
			if (std::regex_search(row, match, myReg))
			{
				row = match[0];
			}
		}
		std::stringstream ss(row);
		
		//input data using delimiter
		while (getline(ss, cell, delimiter)) 
		{
			this->loadData[iteration].push_back(stoi(cell));
			
			if (iteration == numberOfColumns - 1) 
				iteration = 0;
				
			iteration++;
		}
	}
}

void dataManagement::shuffleData()
{
	//get a vector of indexes 
	std::vector<int> indexes;
	indexes.reserve(this->loadData[0].size());

	for (int i = 0; i < this->loadData[0].size(); ++i)
		indexes.push_back(i);

	//shuffle vector of indexes
	std::random_shuffle(indexes.begin(), indexes.end());


	//create a copy load data vector of vectors(inefficient, but works)
	std::vector<std::vector<double>> vCopy = this->loadData;


	int counter = 0;
	//shuffle contents of each vector using shuffled indexes
	for (std::vector<int>::iterator it = indexes.begin(); it != indexes.end(); ++it)
	{
		//for each vector of loadData
		for (int i = 0; i < this->loadData.size(); i++) 
		{
			//use the copy to shuffle the original data
			this->loadData[i].at(counter) = vCopy[i].at(*it);
		}
		counter++;
	}

}

void dataManagement::splitData()
{
	int size = this->loadData[0].size();

	//create a split value which separates test from train
	int firstSplit = std::floor(size * this->TRAIN_SET_PERCENT);
	
	int secondSplit = std::floor(size * this->TEST_SET_PERCENT);

	//value determining amount of elements after split sent to train
	int remainingValues = size - (firstSplit + secondSplit);

	for (int i = 0; i < this->loadData.size(); i++) 
	{
		//push back a new train data vector
		std::vector<double> trainVector (std::floor(this->loadData[i].size() * TRAIN_SET_PERCENT));
		this->trainData.push_back(trainVector);

		std::vector<double>::iterator movingIterator = this->loadData[i].begin();

		//copy contents of first elements determined by ratio to test data
		std::copy_n(movingIterator, firstSplit, this->trainData[i].begin());
		std::advance(movingIterator, firstSplit);

		//push back a new test data vector
		std::vector<double> testVector(std::floor(this->loadData[i].size() * this->TEST_SET_PERCENT));
		this->testData.push_back(testVector);

		//copy remaining contents determined by ratio to train data
		std::copy_n(movingIterator, secondSplit, this->testData[i].begin());
		std::advance(movingIterator, secondSplit);

		//push back a new validation data vector
		std::vector<double> validationVector(std::ceil(this->loadData[i].size() * this->VALIDATION_SET_PERCENT));
		this->validationData.push_back(validationVector);

		std::copy_n(movingIterator, remainingValues, this->validationData[i].begin());

	}
	
}


