#include "LinearRegression.h"

LinearRegression::LinearRegression()
{
}

double LinearRegression::calculateInterceptDerivative(std::vector<double> yPoints)
{
	double interceptDerivative = 0.0;
	int n = yPoints.size();
	for (int j = 0; j < yPoints.size(); j++)
	{
		interceptDerivative += (yPoints.at(j) - this->predictedValues.at(j));
	}
	interceptDerivative = (-2 / (double)n) * interceptDerivative;
	return interceptDerivative;
}

void LinearRegression::calculateMSELoss(std::vector<double> yPoints)
{
	double error = 0;
	for (int j = 0; j < yPoints.size(); j++)
	{
		error += std::pow(yPoints.at(j) - this->predictedValues.at(j), 2);
	}
	this->errors.push_back(error / yPoints.size());
}

double LinearRegression::calculateR2Score(std::vector<double> yPoints)
{
	double SSres = 0.0;
	double SStot = 0.0;
	double yMeanValue = 0.0;
	for (int j = 0; j < yPoints.size(); j++)
	{
		yMeanValue = yPoints[j];
	}
	yMeanValue = yMeanValue / yPoints.size();
	for (int j = 0; j < yPoints.size(); j++)
	{
		SStot += std::pow(yPoints[j] - yMeanValue, 2);
		SSres += std::pow(yPoints[j] - this->predictedValues.at(j), 2);
	}
	return (1 - (SSres / SStot));
}

double LinearRegression::calculateSlopeDerivative(std::vector<double> xPoints, std::vector<double> yPoints)
{
	double slopeDerivative = 0.0;
	int n = xPoints.size();
	for (int j = 0; j < xPoints.size(); j++)
	{
		slopeDerivative += xPoints.at(j) * (yPoints.at(j) - this->predictedValues.at(j));
	}
	slopeDerivative = (-2 / (double)n) * slopeDerivative;
	return slopeDerivative;
}

void LinearRegression::fit(std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	//value resetting after each algorithm call
	if (this->getSlope() != 0)
		this->updateSlope(0.0);
	if (this->getIntercept() != 0)
		this->updateIntercept(0.0);
	if (this->predictedValues.size() > 0)
		this->predictedValues.clear();
	if (this->errors.size() > 0)
		this->errors.clear();

	bool isBestErrorValueFound = 0;
	//run it epoch times
	for (int i = 0; i < this->epochs; i++)
	{
		int batchSize = 8;
		std::cout << "EPOCH: " << i + 1 << std::endl;
		textBoxVector.at(linRegTrainingInfo)->addText("EPOCH: " + std::to_string(i + 1) + "\n");

		for (int j = 0; j < batchSize; j++)
		{
			//calculate prediction
			predict(this->modelTrainingData[0]);
			//calculate loss
			calculateMSELoss(this->modelTrainingData[1]);
			//calculate derivatives
			double slopeDerivative = calculateSlopeDerivative(this->modelTrainingData[0], this->modelTrainingData[1]);
			double interceptDerivative = calculateInterceptDerivative(this->modelTrainingData[1]);
			//update slope and intercept
			updateSlope(getSlope() - (getLearningRate() * slopeDerivative));
			updateIntercept(getIntercept() - (getLearningRate() * interceptDerivative));

			std::cout << "Current slope: " << getSlope() << " Current intercept: " << getIntercept() << " MSE error: " << this->errors.at(j) << std::endl;
			textBoxVector.at(linRegTrainingInfo)->addText("Current slope: " + std::to_string(getSlope()) + " Current intercept: " + std::to_string(getIntercept()) + " MSE error: " + std::to_string(this->errors.at(j)) + "\n");

			double currentError = (int)(this->errors.at(j)*100000.0) / 100000.0;
			if (j > 0)
			{
				double prevError = (int)(this->errors.at(j - 1)*100000.0) / 100000.0;
				if (currentError == prevError)
				{
					std::cout << "The best error value found! Ending the calculations" << std::endl;
					textBoxVector.at(linRegTrainingInfo)->addText("The best error value found! Ending the calculations \n");
					isBestErrorValueFound = true;
					break;
				}
			}

		}
		this->errors.clear();
		if (isBestErrorValueFound)
			break;
	}
	//calculate R2 score
	double R2 = calculateR2Score(this->modelTrainingData[1]);
	std::cout << "R2 score: " << R2 << std::endl;
	textBoxVector.at(linRegTrainingInfo)->addText("R2 score: " + std::to_string(R2) + "\n");

}

int LinearRegression::getEpochs()
{
	return this->epochs;
}

double LinearRegression::getLearningRate()
{
	return this->learningRate;
}

void LinearRegression::predict(std::vector<double> featurePoints)
{
	for (int j = 0; j < featurePoints.size(); j++)
	{
		double predictedValue = this->slope * featurePoints.at(j) + this->intercept;
		if (this->predictedValues.size() <= j)
			this->predictedValues.push_back(predictedValue);
		else
			this->predictedValues.at(j) = predictedValue;
	}
}

void LinearRegression::runAlgorithm(dataManagement &loadedData, std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	this->setTrainingData(loadedData.trainData);
	this->setTestData(loadedData.testData);
	this->setValidationData(loadedData.validationData);
	this->fit(textBoxVector);
	this->testPerformance(textBoxVector);
}

void LinearRegression::setEpochs(tgui::Slider::Ptr slider)
{
	this->epochs = slider->getValue();
}

void LinearRegression::setLearningRate(tgui::Slider::Ptr slider)
{
	this->learningRate = slider->getValue();
}

void LinearRegression::setTestData(std::vector<std::vector<double>> data)
{
	this->modelTestData = data;
}

void LinearRegression::setTrainingData(std::vector<std::vector<double>> data)
{
	this->modelTrainingData = data;
}

void LinearRegression::setValidationData(std::vector<std::vector<double>> data)
{
	this->modelValidationData = data;
}

bool LinearRegression::sortLinearError(double a, double b)
{
	double aValue = abs(a - 0);
	double bValue = abs(b - 0);
	return aValue < bValue;
}

void LinearRegression::testPerformance(std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	std::cout << "Testing performance! " << std::endl;
	textBoxVector.at(linRegTestResults)->addText("Testing performance! \n");
	for (int i = 0; i < modelTestData[0].size(); i++)
	{
		double prediction = getSlope() * this->modelTestData[0][i] + getIntercept();
		std::cout << "X: " << this->modelTestData[0][i] << " predicted Y: " << prediction << std::endl;
		textBoxVector.at(linRegTestResults)->addText("X: " + std::to_string(this->modelTestData[0][i]) + " predicted Y: " + std::to_string(prediction) + "\n");
	}
}

void LinearRegression::updateIntercept(double newIntercept)
{
	this->intercept = newIntercept;
}

void LinearRegression::updateSlope(double newSlope)
{
	this->slope = newSlope;
}