var searchData=
[
  ['getb0',['getB0',['../class_logistic_regression.html#a230314d8d1d19660267edde057f5f207',1,'LogisticRegression']]],
  ['getb1',['getB1',['../class_logistic_regression.html#ac412741dcc5bc782543cb91ea7709099',1,'LogisticRegression']]],
  ['getb2',['getB2',['../class_logistic_regression.html#adbe350994e7b4be51cf64b9a941a6bbc',1,'LogisticRegression']]],
  ['getconvergethreshold',['getConvergeThreshold',['../class_logistic_regression.html#ab7a31ec95d27d0f65b56363cd007eb28',1,'LogisticRegression']]],
  ['getepochs',['getEpochs',['../class_linear_regression.html#a4848f7677645e242722b01bdc65e9da1',1,'LinearRegression']]],
  ['getintercept',['getIntercept',['../class_linear_regression.html#a34046d77963e6f22b3e9f8d73f2a3f44',1,'LinearRegression']]],
  ['getknninstance',['getKNNInstance',['../class_screen.html#a1672ea3269350001579f4144017f6c31',1,'Screen']]],
  ['getlearningrate',['getLearningRate',['../class_linear_regression.html#ae79c6db7b24604b930432de6453acd88',1,'LinearRegression::getLearningRate()'],['../class_logistic_regression.html#a14ef9fbacd39758cfcfab497a5d8a1b7',1,'LogisticRegression::getLearningRate()']]],
  ['getlinreginstance',['getLinRegInstance',['../class_screen.html#a2515ca19683658a44b4ce15052373d96',1,'Screen']]],
  ['getlogreginstance',['getLogRegInstance',['../class_screen.html#a01eb85622ecbbec4aac4de29bbb464e1',1,'Screen']]],
  ['getprobabilitythreshold',['getProbabilityThreshold',['../class_logistic_regression.html#a25756861412bc497ab4822ac0d531798',1,'LogisticRegression']]],
  ['getslope',['getSlope',['../class_linear_regression.html#a40ebf3adc9fd05744126bf57149296bf',1,'LinearRegression']]],
  ['guiloop',['guiLoop',['../class_screen.html#afa9eee0e3536d81242d232324305e1d1',1,'Screen']]]
];
