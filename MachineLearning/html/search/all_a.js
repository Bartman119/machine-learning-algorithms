var searchData=
[
  ['learningrate',['learningRate',['../class_linear_regression.html#a59ba1f31426d5c953d2cab968223e861',1,'LinearRegression::learningRate()'],['../class_logistic_regression.html#a558b95cbf0c94dc4a58e3b0541b0c2ca',1,'LogisticRegression::learningRate()']]],
  ['linearregression',['LinearRegression',['../class_linear_regression.html',1,'LinearRegression'],['../class_linear_regression.html#afeb4c101e4e8b20a75db3d4ce3f1cbc6',1,'LinearRegression::LinearRegression()']]],
  ['linreg',['linReg',['../class_screen.html#a7b73e25604dcdb139e2cf87cbe736295afe4abbff36d6d3ca1275f015cdab8da2',1,'Screen']]],
  ['linregtestresults',['linRegTestResults',['../class_algorithm_strategy.html#a14f67ce707b839c5f0b67c2217b50eb6a178e2df7d00858c6d4a248cbd7fe890b',1,'AlgorithmStrategy']]],
  ['linregtraininginfo',['linRegTrainingInfo',['../class_algorithm_strategy.html#a14f67ce707b839c5f0b67c2217b50eb6a39ec1fcd402ae9a330d687a10c709b5d',1,'AlgorithmStrategy']]],
  ['loadcorrectdata',['loadCorrectData',['../class_screen.html#a8df504d4577b3b4eb4d7a5c665f16afc',1,'Screen']]],
  ['loaddata',['loadData',['../classdata_management.html#abfb80f579498d498accd98a677192dd0',1,'dataManagement']]],
  ['logisticregression',['LogisticRegression',['../class_logistic_regression.html',1,'LogisticRegression'],['../class_logistic_regression.html#ac052292dfeced0b7272e099e5ccb1dc3',1,'LogisticRegression::LogisticRegression()']]],
  ['logreg',['logReg',['../class_screen.html#a7b73e25604dcdb139e2cf87cbe736295a8ca9a74a90e189de334e25b1fa3fd5b6',1,'Screen']]],
  ['logregtestresults',['logRegTestResults',['../class_algorithm_strategy.html#a14f67ce707b839c5f0b67c2217b50eb6a432d39ee6dd0abcd44bd8583da003c64',1,'AlgorithmStrategy']]],
  ['logregtraininginfo',['logRegTrainingInfo',['../class_algorithm_strategy.html#a14f67ce707b839c5f0b67c2217b50eb6a8f94d823c98fd35c32226e2f796eb300',1,'AlgorithmStrategy']]]
];
