var searchData=
[
  ['readfrombmp',['readFromBmp',['../classdata_management.html#a1de7a4bfb6407dd86e8b97e226a8d6c0',1,'dataManagement']]],
  ['readfromcsvortxt',['readFromCsvOrTxt',['../classdata_management.html#a65341c99ee2018c39945d9a4fb57ce7e',1,'dataManagement']]],
  ['runalgorithm',['runAlgorithm',['../class_algorithm_strategy.html#a8f6fc490d2bb591d61c8ab7e88281bfd',1,'AlgorithmStrategy::runAlgorithm()'],['../classknn.html#a7c8f6d0822485f4f7f75b201917dab13',1,'knn::runAlgorithm()'],['../class_linear_regression.html#a655ac0b179a51f52766352986b4b97e1',1,'LinearRegression::runAlgorithm()'],['../class_logistic_regression.html#a7c8ae67f20e725f8263c5e07001db4d7',1,'LogisticRegression::runAlgorithm()'],['../class_screen.html#a5e4b95c9ed661ccc82011e0954c7a749',1,'Screen::runAlgorithm()']]]
];
