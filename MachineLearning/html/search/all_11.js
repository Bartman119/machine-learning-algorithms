var searchData=
[
  ['test_5fset_5fpercent',['TEST_SET_PERCENT',['../classdata_management.html#a674cda4059e4163f8f75bb432db0e7c0',1,'dataManagement']]],
  ['testdata',['testData',['../classdata_management.html#a0a55ea4775328c916bce777b3ce1e52d',1,'dataManagement']]],
  ['testperformance',['testPerformance',['../classknn.html#a9fdb02778204abade072d417edc6af16',1,'knn::testPerformance()'],['../class_linear_regression.html#adedf8edd70972e0e3754972dd29976e8',1,'LinearRegression::testPerformance()'],['../class_logistic_regression.html#abf33e83c43794f77e86799a45edfd072',1,'LogisticRegression::testPerformance()']]],
  ['textboxnumber',['textBoxNumber',['../class_algorithm_strategy.html#a14f67ce707b839c5f0b67c2217b50eb6',1,'AlgorithmStrategy']]],
  ['textboxvector',['textBoxVector',['../class_screen.html#a3ce880814c37010caa6f329e6a0b4b8f',1,'Screen']]],
  ['train',['train',['../classknn.html#abc187bbff039803011171b84bcf6cc20',1,'knn']]],
  ['train_5fset_5fpercent',['TRAIN_SET_PERCENT',['../classdata_management.html#a23e0acb4e63ecf18e457b1b11ed14a07',1,'dataManagement']]],
  ['traindata',['trainData',['../classdata_management.html#aab1efa3cd0b5fde72c2071793acce57f',1,'dataManagement']]]
];
