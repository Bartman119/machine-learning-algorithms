var searchData=
[
  ['screen',['Screen',['../class_screen.html#a87153e7deee7754968c1527e984196fa',1,'Screen']]],
  ['setb0',['setB0',['../class_logistic_regression.html#a54d76616093976656535d942d6e9b2cf',1,'LogisticRegression']]],
  ['setb1',['setB1',['../class_logistic_regression.html#a728765d304088a3eff229881cc9997c0',1,'LogisticRegression']]],
  ['setb2',['setB2',['../class_logistic_regression.html#aac52a7894b5e2bf430dc1bfda475a471',1,'LogisticRegression']]],
  ['setconvergethreshold',['setConvergeThreshold',['../class_logistic_regression.html#a76b0126b1e162ef73f0ae5c360e7874e',1,'LogisticRegression']]],
  ['setepochs',['setEpochs',['../class_linear_regression.html#a5064703d52eaeffe4621d24fb7d99385',1,'LinearRegression']]],
  ['setk',['setK',['../classknn.html#a1913fbdfa01f46f6549aa2e8968cc05f',1,'knn']]],
  ['setlearningrate',['setLearningRate',['../class_linear_regression.html#a6a1c039efa1f09aad4af46c332cbae41',1,'LinearRegression::setLearningRate()'],['../class_logistic_regression.html#a09f4159f5ca6a3366166753a8c8fa976',1,'LogisticRegression::setLearningRate()']]],
  ['setprobabilitythreshold',['setProbabilityThreshold',['../class_logistic_regression.html#ab79fd8f4e4da450009bb84700856456f',1,'LogisticRegression']]],
  ['settestdata',['setTestData',['../class_algorithm_strategy.html#af810992cff40c263c2f51764823a7512',1,'AlgorithmStrategy::setTestData()'],['../classknn.html#abf51290c654ff0d65bb4e8feed7fe53f',1,'knn::setTestData()'],['../class_linear_regression.html#a39b8e7330db1a223f2cde2d938cc2985',1,'LinearRegression::setTestData()'],['../class_logistic_regression.html#ac6709f7be4c7f00d931c96bd6af61adb',1,'LogisticRegression::setTestData()']]],
  ['settrainingdata',['setTrainingData',['../class_algorithm_strategy.html#a6d6c406972c32794c52724d406357794',1,'AlgorithmStrategy::setTrainingData()'],['../classknn.html#a32f86953089fbb6948c6c5f4a74d46af',1,'knn::setTrainingData()'],['../class_linear_regression.html#ae20b7757ef94c68195a508f0a4cc7213',1,'LinearRegression::setTrainingData()'],['../class_logistic_regression.html#ad286bff90149d1d7130d30c0e9b5a62f',1,'LogisticRegression::setTrainingData()']]],
  ['setvalidationdata',['setValidationData',['../class_algorithm_strategy.html#a99f3dca0f86777de2e4a82e1967a5732',1,'AlgorithmStrategy::setValidationData()'],['../classknn.html#a8ccbe6de819f430bc1d947824eb40fee',1,'knn::setValidationData()'],['../class_linear_regression.html#a15ccd5a3244731d2298217eb96219119',1,'LinearRegression::setValidationData()'],['../class_logistic_regression.html#a50742a4b79757c8c9a2b89536b38b5c6',1,'LogisticRegression::setValidationData()']]],
  ['shuffledata',['shuffleData',['../classdata_management.html#a4c5d4b9dcc486eb6395ee43514a2ec4d',1,'dataManagement']]],
  ['sortlinearerror',['sortLinearError',['../class_linear_regression.html#a3b74850511f9e4ac17342a2899d3814d',1,'LinearRegression']]],
  ['splitdata',['splitData',['../classdata_management.html#a86c72f0e76cb2ff46275f84870bed1d8',1,'dataManagement']]]
];
