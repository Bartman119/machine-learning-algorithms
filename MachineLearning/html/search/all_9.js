var searchData=
[
  ['k',['k',['../classknn.html#a8ae39e04827907ed414503ea0da0bf70',1,'knn']]],
  ['knn',['knn',['../classknn.html',1,'knn'],['../class_screen.html#a7b73e25604dcdb139e2cf87cbe736295a3025ef4ec6bb5ad4b000d177960d69d7',1,'Screen::KNN()'],['../classknn.html#a4605abb46d92bdb27359b4d1e8a7e3bf',1,'knn::knn(int)'],['../classknn.html#a88eda798cbe0b082429cec0e6f2d9948',1,'knn::knn()']]],
  ['knntestresults',['KNNTestResults',['../class_algorithm_strategy.html#a14f67ce707b839c5f0b67c2217b50eb6aff0c31b3f555204a3c49cfe9177d40eb',1,'AlgorithmStrategy']]],
  ['knntraininginfo',['KNNTrainingInfo',['../class_algorithm_strategy.html#a14f67ce707b839c5f0b67c2217b50eb6ab613a11cde7f1d3213c1af3b88d95e6a',1,'AlgorithmStrategy']]]
];
