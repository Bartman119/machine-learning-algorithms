var searchData=
[
  ['mainscreen',['mainScreen',['../class_screen.html#a7b73e25604dcdb139e2cf87cbe736295af9f9e12ce803f03bfdfd6b71f0a28f00',1,'Screen']]],
  ['modeltestdata',['modelTestData',['../classknn.html#ae859ce1b7b41d31f180cc9ad6cd7762f',1,'knn::modelTestData()'],['../class_linear_regression.html#a3e4065808185f45cf8059e6b3caf01c6',1,'LinearRegression::modelTestData()'],['../class_logistic_regression.html#aac187b4e5ea50fb674d99c5410c120cd',1,'LogisticRegression::modelTestData()']]],
  ['modeltrainingdata',['modelTrainingData',['../classknn.html#ae159409da553641e83b4442e9ade12c5',1,'knn::modelTrainingData()'],['../class_linear_regression.html#ad8bb23de5c755d7bd80a03dd6453d6ef',1,'LinearRegression::modelTrainingData()'],['../class_logistic_regression.html#ab6cb2a8387ecdace4c223a165334654b',1,'LogisticRegression::modelTrainingData()']]],
  ['modelvalidationdata',['modelValidationData',['../classknn.html#ac5a0d366e7be4c38405dde618a9d8738',1,'knn::modelValidationData()'],['../class_linear_regression.html#a5b8ce877cb89d5c3af6c35d683163f5a',1,'LinearRegression::modelValidationData()'],['../class_logistic_regression.html#ab0b22a6380924312586c0ac3ece1beda',1,'LogisticRegression::modelValidationData()']]]
];
