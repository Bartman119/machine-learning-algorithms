#pragma once
#ifndef ALGORITHM_STRATEGY_H
#define ALGORITHM_STRATEGY_H
#include "dataManagement.h"
#include <TGUI/TGUI.hpp>

/**
*	Algorithm strategy class.
*	A parent class for each Machine Learning algorithm class.
*	Used for strategy pattern utilization
*/
class AlgorithmStrategy 
{
public:
	/**
	* textBoxNumber enum structure.
	* Contains values connected to textBoxes in each algorithm screen.
	*/
	enum textBoxNumber
	{
		linRegTrainingInfo = 0, /**< Value corresponding to textBox responsible for Linear Regression algorithm training output. */
		linRegTestResults, /**< Value corresponding to textBox responsible for Linear Regression algorithm testing output. */
		logRegTrainingInfo, /**< Value corresponding to textBox responsible for Logistic Regression algorithm training output. */
		logRegTestResults, /**< Value corresponding to textBox responsible for Logistic Regression algorithm testing output. */
		KNNTrainingInfo, /**< Value corresponding to textBox responsible for KNN algorithm training output. */
		KNNTestResults /**< Value corresponding to textBox responsible for KNN algorithm testing output. */
	};

	/**
	* Pure virtual member for executing selected algorithm.
	* @param loadedData dataManagement class reference used to pass data from a file to an algorithm class.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	* @see dataManagement
	*/
	virtual void runAlgorithm(dataManagement & loadedData, std::vector<tgui::TextBox::Ptr> textBoxVector);

	/**
	*  Pure virtual training data setter for a given algorithm
	*/
	virtual void setTrainingData();
	
	/**
	*  Pure virtual test data setter for a given algorithm
	*/
	virtual void setTestData();
	
	/**
	*  Pure virtual validation data setter for a given algorithm
	*/
	virtual void setValidationData();


	/**
	*  Pure virtual method used for training the model
	*/
	virtual void fit();
};


#endif