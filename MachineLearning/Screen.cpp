#include "Screen.h"

Screen::Screen(dataManagement loadedData):_loadedData(loadedData)
{
	//create a window and set a theme
	window.create(sf::VideoMode(800, 600), "TGUI window");
	gui.setTarget(window);

	tgui::Theme theme{ "./TGUI-0.8/themes/Black.txt" };
	this->basicTheme = theme;
}

void Screen::chooseScreenToDisplay(int screenPosition)
{
	for (int i = 0; i < this->screenVector.size(); i++)
	{
		if (i == screenPosition)
			this->screenVector.at(i)->moveToFront();
		else
			this->screenVector.at(i)->moveToBack();
	}
}
void Screen::createKNNScreen()
{
	tgui::Panel::Ptr knnScreen = tgui::Panel::create();
	knnScreen->setSize({ "100%","100%" });

	//2 hyperparameter sliders and labels, 2 textboxes and 2 buttons
	tgui::Slider::Ptr kSlider = tgui::Slider::create();
	kSlider->setSize({ "20%", "5%" });
	kSlider->setPosition({ "20% - (size/2)","20%" });
	kSlider->setMaximum(21);
	kSlider->setMinimum(1);
	kSlider->setStep(1);
	kSlider->setValue(1);

	tgui::Label::Ptr kLabel = tgui::Label::create();
	kLabel->setText("K value");
	kLabel->setPosition({ bindLeft(kSlider) + "10% -(size/2)",bindTop(kSlider) - "5%" });
	knnScreen->add(kLabel);

	tgui::Label::Ptr kDisplay = tgui::Label::create();
	std::string kValue = std::to_string(kSlider->getValue());
	kDisplay->setText(kValue);
	kDisplay->setPosition({ bindRight(kSlider) + "3%", bindTop(kSlider) });
	knnScreen->add(kDisplay);

	//kSlider signals
	kSlider->connect("ValueChanged", &Screen::updateSliderDisplay, this, kSlider, kDisplay);
	kSlider->connect("ValueChanged", &knn::setK, std::ref(Screen::_knnInstance), kSlider);
	knnScreen->add(kSlider);

	tgui::TextBox::Ptr trainingInfo = tgui::TextBox::create();
	trainingInfo->setSize({ "40%","50%" });
	trainingInfo->setPosition({ "50%","10%" });
	trainingInfo->setReadOnly(true);
	this->textBoxVector.push_back(trainingInfo);
	knnScreen->add(trainingInfo);

	tgui::TextBox::Ptr testResults = tgui::TextBox::create();
	testResults->setSize({ "80%","20%" });
	testResults->setPosition({ "10%","70%" });
	testResults->setReadOnly(true);
	this->textBoxVector.push_back(testResults);
	knnScreen->add(testResults);

	tgui::Button::Ptr runButton = tgui::Button::create();
	runButton->setText("Run the prediciton!");
	runButton->setPosition({ bindLeft(kSlider) + "10% -(size/2)", bindBottom(kSlider) - "20%" });
	//run the algorithm signal
	runButton->connect("Pressed", &Screen::runAlgorithm, this, std::ref(this->_knnInstance), std::ref(this->_loadedData));
	knnScreen->add(runButton);

	tgui::Button::Ptr returnButton = tgui::Button::create();
	returnButton->setText("Return to menu");
	returnButton->setPosition({ "50% - (size/2)", bindBottom(testResults) + "5%" });
	//button signal
	returnButton->connect("Pressed", &Screen::chooseScreenToDisplay, this, screenNumber::mainScreen);
	knnScreen->add(returnButton);
	gui.add(knnScreen);
	this->screenVector.push_back(knnScreen);
}

void Screen::createLinRegScreen()
{
	tgui::Panel::Ptr linRegScreen = tgui::Panel::create();
	linRegScreen->setSize({ "100%","100%" });

	//2 hyperparameter sliders and labels, 2 textboxes and 2 buttons
	tgui::Slider::Ptr epochSlider = tgui::Slider::create();
	epochSlider->setSize({ "20%", "5%" });
	epochSlider->setPosition({ "20% - (size/2)","20%" });
	epochSlider->setMaximum(100);
	epochSlider->setMinimum(1);
	epochSlider->setValue(1);
	epochSlider->setStep(1);

	tgui::Label::Ptr epochLabel = tgui::Label::create();
	epochLabel->setText("Epochs");
	epochLabel->setPosition({ bindLeft(epochSlider) + "10% -(size/2)",bindTop(epochSlider) - "5%" });
	linRegScreen->add(epochLabel);

	tgui::Label::Ptr epochDisplay = tgui::Label::create();
	std::string epochValue = std::to_string(epochSlider->getValue());
	epochDisplay->setText(epochValue);
	epochDisplay->setPosition({ bindRight(epochSlider) + "3%", bindTop(epochSlider) });
	linRegScreen->add(epochDisplay);

	//epochSlider signals
	epochSlider->connect("ValueChanged", &Screen::updateSliderDisplay, this, epochSlider, epochDisplay);
	epochSlider->connect("ValueChanged", &LinearRegression::setEpochs, std::ref(Screen::_linRegInstance), epochSlider);
	linRegScreen->add(epochSlider);

	tgui::Slider::Ptr learningRateSlider = tgui::Slider::create();
	learningRateSlider->setSize({ "20%", "5%" });
	learningRateSlider->setPosition({ bindLeft(epochSlider),bindTop(epochSlider) + "20%" });
	learningRateSlider->setMaximum(3);
	learningRateSlider->setMinimum(0.001);
	learningRateSlider->setValue(0.001);
	learningRateSlider->setStep(0.001);

	tgui::Label::Ptr learningRateLabel = tgui::Label::create();
	learningRateLabel->setText("Learning Rate");
	learningRateLabel->setPosition({ bindLeft(learningRateSlider) + "10% -(size/2)",bindTop(learningRateSlider) - "5%" });
	linRegScreen->add(learningRateLabel);

	tgui::Label::Ptr learningRateDisplay = tgui::Label::create();
	std::string learningRateValue = std::to_string(learningRateSlider->getValue());
	learningRateDisplay->setText(learningRateValue);
	learningRateDisplay->setPosition({ bindRight(learningRateSlider) + "3%", bindTop(learningRateSlider) });
	linRegScreen->add(learningRateDisplay);

	//learningRateSlider signals
	learningRateSlider->connect("ValueChanged", &Screen::updateSliderDisplay, this, learningRateSlider, learningRateDisplay);
	learningRateSlider->connect("ValueChanged", &LinearRegression::setLearningRate, std::ref(Screen::_linRegInstance), learningRateSlider);
	linRegScreen->add(learningRateSlider);

	tgui::TextBox::Ptr trainingInfo = tgui::TextBox::create();
	trainingInfo->setSize({ "40%","50%" });
	trainingInfo->setPosition({ "50%","10%" });
	trainingInfo->setReadOnly(true);
	this->textBoxVector.push_back(trainingInfo);
	linRegScreen->add(trainingInfo);

	tgui::TextBox::Ptr testResults = tgui::TextBox::create();
	testResults->setSize({ "80%","20%" });
	testResults->setPosition({ "10%","70%" });
	testResults->setReadOnly(true);
	this->textBoxVector.push_back(testResults);
	linRegScreen->add(testResults);

	tgui::Button::Ptr runButton = tgui::Button::create();
	runButton->setText("Run the prediciton!");
	runButton->setPosition({ bindLeft(epochSlider) + "10% -(size/2)", bindBottom(epochSlider) - "20%" });
	//run the algorithm signal
	runButton->connect("Pressed", &Screen::runAlgorithm, this, std::ref(this->_linRegInstance), std::ref(this->_loadedData));
	linRegScreen->add(runButton);

	tgui::Button::Ptr returnButton = tgui::Button::create();
	returnButton->setText("Return to menu");
	returnButton->setPosition({ "50% - (size/2)", bindBottom(testResults) + "5%" });
	//button signal
	returnButton->connect("Pressed", &Screen::chooseScreenToDisplay, this, screenNumber::mainScreen);
	linRegScreen->add(returnButton);
	gui.add(linRegScreen);
	this->screenVector.push_back(linRegScreen);
}

void Screen::createLogRegScreen()
{
	tgui::Panel::Ptr logRegScreen = tgui::Panel::create();
	logRegScreen->setSize({ "100%","100%" });

	//2 hyperparameter sliders and labels, 2 textboxes and 2 buttons
	tgui::Slider::Ptr convergeThresholdSlider = tgui::Slider::create();
	convergeThresholdSlider->setSize({ "20%", "5%" });
	convergeThresholdSlider->setPosition({ "20% - (size/2)","20%" });
	convergeThresholdSlider->setMaximum(0.1);
	convergeThresholdSlider->setMinimum(0.0001);
	convergeThresholdSlider->setStep(0.0001);
	convergeThresholdSlider->setValue(0.0001);

	tgui::Label::Ptr convergeThresholdLabel = tgui::Label::create();
	convergeThresholdLabel->setText("Converge Threshold");
	convergeThresholdLabel->setPosition({ bindLeft(convergeThresholdSlider) + "10% -(size/2)",bindTop(convergeThresholdSlider) - "5%" });
	logRegScreen->add(convergeThresholdLabel);

	tgui::Label::Ptr convergeThresholdDisplay = tgui::Label::create();
	std::string convergeThresholdValue = std::to_string(convergeThresholdSlider->getValue());
	convergeThresholdDisplay->setText(convergeThresholdValue);
	convergeThresholdDisplay->setPosition({ bindRight(convergeThresholdSlider) + "3%", bindTop(convergeThresholdSlider) });
	logRegScreen->add(convergeThresholdDisplay);

	//convergeThresholdSlider signals
	convergeThresholdSlider->connect("ValueChanged", &Screen::updateSliderDisplay, this, convergeThresholdSlider, convergeThresholdDisplay);
	convergeThresholdSlider->connect("ValueChanged", &LogisticRegression::setConvergeThreshold, std::ref(Screen::_logRegInstance), convergeThresholdSlider);
	logRegScreen->add(convergeThresholdSlider);

	tgui::Slider::Ptr learningRateSlider = tgui::Slider::create();
	learningRateSlider->setSize({ "20%", "5%" });
	learningRateSlider->setPosition({ bindLeft(convergeThresholdSlider),bindTop(convergeThresholdSlider) + "20%" });
	learningRateSlider->setMaximum(1.2);
	learningRateSlider->setMinimum(0.001);
	learningRateSlider->setStep(0.001);
	learningRateSlider->setValue(0.001);

	tgui::Label::Ptr learningRateLabel = tgui::Label::create();
	learningRateLabel->setText("Learning Rate");
	learningRateLabel->setPosition({ bindLeft(learningRateSlider) + "10% -(size/2)",bindTop(learningRateSlider) - "5%" });
	logRegScreen->add(learningRateLabel);

	tgui::Label::Ptr learningRateDisplay = tgui::Label::create();
	std::string learningRateValue = std::to_string(learningRateSlider->getValue());
	learningRateDisplay->setText(learningRateValue);
	learningRateDisplay->setPosition({ bindRight(learningRateSlider) + "3%", bindTop(learningRateSlider) });
	logRegScreen->add(learningRateDisplay);

	//learningRateSlider signals
	learningRateSlider->connect("ValueChanged", &Screen::updateSliderDisplay, this, learningRateSlider, learningRateDisplay);
	learningRateSlider->connect("ValueChanged", &LogisticRegression::setLearningRate, std::ref(Screen::_logRegInstance), learningRateSlider);
	logRegScreen->add(learningRateSlider);

	tgui::Slider::Ptr probabilityThresholdSlider = tgui::Slider::create();
	probabilityThresholdSlider->setSize({ "20%", "5%" });
	probabilityThresholdSlider->setPosition({ bindLeft(learningRateSlider),bindTop(learningRateSlider) + "20%" });
	probabilityThresholdSlider->setMaximum(0.9);
	probabilityThresholdSlider->setMinimum(0.1);
	probabilityThresholdSlider->setStep(0.01);

	tgui::Label::Ptr probabilityThresholdLabel = tgui::Label::create();
	probabilityThresholdLabel->setText("Probability Threshold");
	probabilityThresholdLabel->setPosition({ bindLeft(probabilityThresholdSlider) + "10% -(size/2)",bindTop(probabilityThresholdSlider) - "5%" });
	logRegScreen->add(probabilityThresholdLabel);

	tgui::Label::Ptr probabilityThresholdDisplay = tgui::Label::create();
	std::string probabilityThresholdValue = std::to_string(probabilityThresholdSlider->getValue());
	probabilityThresholdDisplay->setText(probabilityThresholdValue);
	probabilityThresholdDisplay->setPosition({ bindRight(probabilityThresholdSlider) + "3%", bindTop(probabilityThresholdSlider) });
	logRegScreen->add(probabilityThresholdDisplay);

	//probabilityThresholdSlider signals
	probabilityThresholdSlider->connect("ValueChanged", &Screen::updateSliderDisplay, this, probabilityThresholdSlider, probabilityThresholdDisplay);
	probabilityThresholdSlider->connect("ValueChanged", &LogisticRegression::setLearningRate, std::ref(Screen::_logRegInstance), probabilityThresholdSlider);
	logRegScreen->add(probabilityThresholdSlider);

	tgui::TextBox::Ptr trainingInfo = tgui::TextBox::create();
	trainingInfo->setSize({ "40%","50%" });
	trainingInfo->setPosition({ "50%","10%" });
	trainingInfo->setReadOnly(true);
	this->textBoxVector.push_back(trainingInfo);
	logRegScreen->add(trainingInfo);

	tgui::TextBox::Ptr testResults = tgui::TextBox::create();
	testResults->setSize({ "80%","20%" });
	testResults->setPosition({ "10%","70%" });
	testResults->setReadOnly(true);
	this->textBoxVector.push_back(testResults);
	logRegScreen->add(testResults);

	tgui::Button::Ptr runButton = tgui::Button::create();
	runButton->setText("Run the prediciton!");
	runButton->setPosition({ bindLeft(convergeThresholdSlider) + "10% -(size/2)", bindBottom(convergeThresholdSlider) - "20%" });
	//run the algorithm signal
	runButton->connect("Pressed", &Screen::runAlgorithm, this, std::ref(this->_logRegInstance), std::ref(this->_loadedData));
	logRegScreen->add(runButton);

	tgui::Button::Ptr returnButton = tgui::Button::create();
	returnButton->setText("Return to menu");
	returnButton->setPosition({ "50% - (size/2)", bindBottom(testResults) + "5%" });
	//button signal
	returnButton->connect("Pressed", &Screen::chooseScreenToDisplay, this, screenNumber::mainScreen);
	logRegScreen->add(returnButton);
	gui.add(logRegScreen);
	this->screenVector.push_back(logRegScreen);
}

void Screen::createStartingScreen()
{
	tgui::Panel::Ptr startPanel = tgui::Panel::create();
	startPanel->setSize("100%", "100%");;

	tgui::Button::Ptr startButton = tgui::Button::create();
	startButton->setPosition("50% - (size/2)", "70%");
	startButton->setText("Click here to start!");
	startButton->connect("Pressed", &Screen::chooseScreenToDisplay, this, screenNumber::mainScreen);

	tgui::Label::Ptr title = tgui::Label::create();
	title->setText("Machine Learning Project");
	title->setTextSize(20);
	title->setPosition("50% - (size/2)", "30%");

	startPanel->add(title);
	startPanel->add(startButton);
	gui.add(startPanel);
	this->screenVector.push_back(startPanel);
}

void Screen::createMainScreen()
{
	tgui::Panel::Ptr mainScreen = tgui::Panel::create();
	mainScreen->setSize("100%", "100%");

	tgui::EditBox::Ptr datasetDirectoryInput = tgui::EditBox::create();
	datasetDirectoryInput->setDefaultText("Input dataset name with extension (has to be in Datasets folder!");
	datasetDirectoryInput->setPosition({ "50% - (size/2)", "30%" });
	datasetDirectoryInput->setSize({ "30%","5%" });
	mainScreen->add(datasetDirectoryInput);

	tgui::Button::Ptr linRegButton = tgui::Button::create();
	linRegButton->setText("Linear Regression");
	linRegButton->setPosition({ "16% -(size/2)", "70%" });
	
	//button signals
	linRegButton->connect("Pressed",&Screen::loadCorrectData, this, datasetDirectoryInput);
	linRegButton->connect("Pressed", &Screen::chooseScreenToDisplay, this, screenNumber::linReg);
	mainScreen->add(linRegButton);

	tgui::Button::Ptr logRegButton = tgui::Button::create();
	logRegButton->setText("Logistic Regression");
	logRegButton->setPosition({bindLeft(linRegButton) + "33%", bindTop(linRegButton) });
	
	//button signals
	logRegButton->connect("Pressed",&Screen::loadCorrectData, this, datasetDirectoryInput);
	logRegButton->connect("Pressed", &Screen::chooseScreenToDisplay, this, screenNumber::logReg);
	mainScreen->add(logRegButton);

	tgui::Button::Ptr knnButton = tgui::Button::create();
	knnButton->setText("KNN");
	knnButton->setPosition({ bindLeft(logRegButton) + "33%", bindTop(logRegButton) });
	
	//button signals
	knnButton->connect("Pressed",&Screen::loadCorrectData, this, datasetDirectoryInput);
	knnButton->connect("Pressed", &Screen::chooseScreenToDisplay, this, screenNumber::KNN);
	mainScreen->add(knnButton);

	gui.add(mainScreen);
	this->screenVector.push_back(mainScreen);
}

void Screen::guiLoop()
{
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			// When the window is closed, the application ends
			if (event.type == sf::Event::Closed)
				window.close();

			// When the window is resized, the view is changed
			else if (event.type == sf::Event::Resized)
			{
				window.setView(sf::View(sf::FloatRect(0.f, 0.f, static_cast<float>(event.size.width), static_cast<float>(event.size.height))));
				gui.setView(window.getView());
			}

			// Pass the event to all the widgets
			gui.handleEvent(event);

		}
		window.clear();
		gui.draw();
		window.display();
	}
}

void Screen::loadCorrectData(tgui::EditBox::Ptr inputFile)
{
	std::string dataFile = inputFile->getText().toAnsiString();
	std::string s = "../MachineLearning/Datasets/";
	if (dataFile.find("bmp", dataFile.size() - 4) != std::string::npos)
	{
		s.append(dataFile);
		char filename[] = "../MachineLearning/Datasets/logisticDataset.bmp";
		char * pFilename = filename;
		this->_loadedData.readFromBmp(pFilename);
		this->_loadedData.optimiseData(false);
	}
	else if (dataFile.find("csv", dataFile.size() - 4) != std::string::npos || dataFile.find("txt", dataFile.size() - 4) != std::string::npos)
	{
		s.append(dataFile);
		this->_loadedData.readFromCsvOrTxt(s, ';', 3);
		this->_loadedData.optimiseData(false);
	}
	else
		throw std::invalid_argument("Incorrect directory given!");

}

void Screen::runAlgorithm(AlgorithmStrategy & as, dataManagement & loadedData)
{
	as.runAlgorithm(loadedData, this->textBoxVector);
}

void Screen::updateSliderDisplay(tgui::Slider::Ptr dataSlider, tgui::Label::Ptr outputLabel)
{
	std::string currentValue = std::to_string(dataSlider->getValue());
	outputLabel->setText(currentValue);
}