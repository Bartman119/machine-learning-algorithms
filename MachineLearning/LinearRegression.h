#pragma once
#ifndef LINEAR_REGRESSION_H
#define LINEAR_REGRESSION_H
#include <vector>
#include <algorithm>
#include <iostream>
#include "AlgorithmStrategy.h"
#include "dataManagement.h"

/**
* @brief Linear Regression algorithm class.
* Derived from AlgorithmStrategy class.
* Used for utilization of Linear Regression algorithm in the program. Part of a strategy pattern.
* @see AlgorithmStrategy
*/
class LinearRegression: public AlgorithmStrategy 
{
public:

	/**
	* Vector of doubles storing mean squared error of a dataset.
	*/
	std::vector<double> errors;

	/**
	* Vector of double vectors storing data used for testing of a model.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelTestData;

	/**
	* Vector of double vectors storing training data of a model.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelTrainingData;

	/**
	* Vector of double vectors storing data used for model validation.
	* Contains both features and a label, each in seperate sub vector.
	*/
	std::vector<std::vector<double>> modelValidationData;
	
	/**
	* Vector of doubles storing predicted values for each data point.
	*/
	std::vector<double> predictedValues;

	/**
	* Parameter used as a bias in linear equasion.
	*/
	double intercept = 0.0;

	/**
	* Parameter used as a x coordinate coefficient in linear equasion.
	*/
	double slope = 0.0;

	/**
	* Hyperparameter denoting how many iterations through a dataset algorithm should do.
	*/
	int epochs = 0;

	/**
	* Hyperparameter used in gradient descent to determine the step of a function.
	*/
	double learningRate = 0.0;

	/**
	* Default constructor.
	*/
	LinearRegression();
	
	/**
	* Default destructor.
	*/
	~LinearRegression() = default;

	/**
	* Calculates derivative of an Intercept variable
	* @param yPoints Vector of doubles of y coordinates.
	* @return Calculated derivative of intercept.
	*/
	double calculateInterceptDerivative(std::vector<double> yPoints);
	
	/**
	* Calculates mean squared error of an iteration.
	* @param yPoints Vector of doubles of y coordinates.
	*/
	void calculateMSELoss(std::vector<double> yPoints);

	/**
	* Calculates R2 score of an iteration.
	* @param yPoints Vector of doubles of y coordinates.
	*/
	double calculateR2Score(std::vector<double> yPoints);
	
	/**
	* Calculates derivative of a slope variable.
	* @param xPoints Vector of doubles of x coordinates.
	* @param yPoints Vector of doubles of y coordinates.
	*/
	double calculateSlopeDerivative(std::vector<double> xPoints, std::vector<double> yPoints);
	
	/**
	* Overridden method that trains the model by calculating slope and intercept of a linear function over a training dataset.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	*/
	void fit(std::vector<tgui::TextBox::Ptr> textBoxVector);

	/**
	* Getter returning epoch variable.
	* @return Epoch variable.	
	*/
	int getEpochs();
	
	/**
	* Getter returning intercept variable.
	* @return Intercept variable.
	*/
	double getIntercept() { return intercept; };
	
	/**
	* Getter returning learning rate variable.
	* @return Learning rate variable.
	*/
	double getLearningRate();

	/**
	* Getter returning slope variable.
	* @return Slope variable.
	*/
	double getSlope() { return slope; };
	
	/**
	* Predicts the value of each data point using calculated coefficients.
	* @param featurePoints Vector of doubles of x coordinates.
	*/
	void predict(std::vector<double> featurePoints);

	/**
	* Overriden wrapper method calling all LinearRegression methods to execute an algorithm.
	* @param loadedData dataManagement class pointer containing dataset for an algorithm to operate on.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	* @see setTrainingData()
	* @see setTestData()
	* @see setValidationData()
	* @see fit()
	* @see testPerformance()
	*/
	void runAlgorithm(dataManagement &loadedData, std::vector<tgui::TextBox::Ptr> textBoxVector);
	
	/**
	* Setter modifying epoch variable.
	* @param slider Tgui slider from which current value of a hyperparameter is taken.
	*/
	void setEpochs(tgui::Slider::Ptr slider);

	/**
	* Setter modifying learning rate variable.
	* @param slider Tgui slider from which current value of a hyperparameter is taken.
	*/
	void setLearningRate(tgui::Slider::Ptr slider);

	/**
	* Test data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setTestData(std::vector<std::vector<double>> data);
	
	/**
	* Training data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setTrainingData(std::vector<std::vector<double>> data);
	
	/**
	* Validation data container setter.
	* @param data Vector of double vectors of data points.
	*/
	void setValidationData(std::vector<std::vector<double>> data);
	
	/**
	* Returns true whether second value of linear error is greater from the first one.
	* @param a first double value.
	* @param b second double value.
	* @return Boolean value of the condition.
	*/
	bool sortLinearError(double a, double b);

	/**
	* Method predicting y values of test data points after coefficient modification.
	* @param textBoxVector Vector of tgui textBoxes used to output information during training and testing to gui.
	*/
	void testPerformance(std::vector<tgui::TextBox::Ptr> textBoxVector);

	/**
	* Setter modifying intercept variable.
	* @param newIntercept New double intercept variable.
	*/
	void updateIntercept(double newIntercept);

	/**
	* Setter modifying slope variable.
	* @param newIntercept New double slope variable.
	*/
	void updateSlope(double newSlope);
	
};

#endif LINEAR_REGRESSION_H