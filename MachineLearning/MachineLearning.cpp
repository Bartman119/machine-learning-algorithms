﻿// MachineLearning.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include "dataManagement.h"
#include "KNN.h"
#include "LinearRegression.h"
#include "LogisticRegression.h"
#include "Screen.h"
#include <TGUI/TGUI.hpp>

int main()
{
	dataManagement myData;
	Screen ScreenHandler(myData);
	try
	{
		ScreenHandler.createStartingScreen();
		ScreenHandler.createMainScreen();
		ScreenHandler.createLinRegScreen();
		ScreenHandler.createLogRegScreen();
		ScreenHandler.createKNNScreen();
	}
	catch (const tgui::Exception& e) 
	{
		std::cerr << "TGUI Exception: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}
	
	// Main loop
	try 
	{
		ScreenHandler.chooseScreenToDisplay(ScreenHandler.screenNumber::startingScreen);
		ScreenHandler.guiLoop();
	}
	catch (std::invalid_argument & e)
	{
		std::cout << e.what();
		return 0;
	}
	return 0;
}
