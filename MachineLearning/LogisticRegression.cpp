#include "LogisticRegression.h"
LogisticRegression::LogisticRegression()
{
}

double LogisticRegression::calculateB0Derivative(std::vector<double> featureValues)
{
	double B0Derivative = 0.0;
	for (int j = 0; j < featureValues.size(); j++)
	{
		B0Derivative += (this->predictedValues.at(j) - featureValues.at(j)) * 1.0;
	}
	return B0Derivative / featureValues.size();
}

double LogisticRegression::calculateB1Derivative(std::vector<double> xPoints, std::vector<double> featureValues)
{
	double B1Derivative = 0.0;
	for (int j = 0; j < featureValues.size(); j++)
	{
		B1Derivative += (this->predictedValues.at(j) - featureValues.at(j)) * xPoints.at(j);
	}
	return B1Derivative / featureValues.size();
}

double LogisticRegression::calculateB2Derivative(std::vector<double> yPoints, std::vector<double> featureValues)
{
	double B2Derivative = 0.0;
	for (int j = 0; j < featureValues.size(); j++)
	{
		B2Derivative += (this->predictedValues.at(j) - featureValues.at(j)) * yPoints.at(j);
	}
	return B2Derivative / featureValues.size();
}

double LogisticRegression::calculateCostFunction(std::vector<double> featureValues)
{
	double costFunction = 0.0;
	for (int j = 0; j < this->predictedValues.size(); j++)
	{
		costFunction += ((-(featureValues.at(j)) * log10(this->predictedValues.at(j))) - ((1 - featureValues.at(j)) * log10(1 - this->predictedValues.at(j))));
	}
	return costFunction / (double)predictedValues.size();
}

void LogisticRegression::fit(std::vector<tgui::TextBox::Ptr> textBoxVector)
{

	std::cout << "Training data!" << std::endl;
	textBoxVector.at(logRegTrainingInfo)->addText("Training data! \n");
	//initial values and first model prediciton
	setB0(0.0);
	setB1(0.0);
	setB2(0.0);
	predict(this->modelTrainingData[1], this->modelTrainingData[0]);
	double cost = calculateCostFunction(this->modelTrainingData[2]);
	double costChange = 0.0;
	double oldCost = 0.0;
	while (costChange < this->convergeThreshold)
	{
		oldCost = cost;
		//calculate derivatives
		double b0Derivative = calculateB0Derivative(this->modelTrainingData[2]);
		double b1Derivative = calculateB1Derivative(this->modelTrainingData[0], this->modelTrainingData[2]);
		double b2Derivative = calculateB2Derivative(this->modelTrainingData[1], this->modelTrainingData[2]);

		//update b parameters
		setB0(getB0() - (getLearningRate() * b0Derivative));
		setB1(getB1() - (getLearningRate() * b1Derivative));
		setB2(getB2() - (getLearningRate() * b2Derivative));
		this->predictedValues.clear();

		//calculate new cost after b parameter update
		predict(this->modelTrainingData[1], this->modelTrainingData[0]);
		cost = calculateCostFunction(this->modelTrainingData[2]);
		if (((int)(costChange*100000.0) / 100000.0) == ((int)((std::abs(oldCost - cost))*100000.0) / 100000.0))
		{
			std::cout << "Insignificant cost change! Finishing calculations \n";
			textBoxVector.at(logRegTrainingInfo)->addText("Insignificant cost change! Finishing calculations \n");
			break;
		}
		costChange = std::abs(oldCost - cost);

		//output information
		std::cout << "Current b2 value: " << getB2() << " Current b1 value: " << getB1() << " Current b0 value: " << getB0() << " Cost change: " << costChange << std::endl;
		textBoxVector.at(logRegTrainingInfo)->addText("Current b2 value: " + std::to_string(getB2()) + " Current b1 value: " + std::to_string(getB1()) + " Current b0 value: " + std::to_string(getB0()) + " Cost change: " + std::to_string(costChange) + "\n");
	}
	this->predictedValues.clear();
}

double LogisticRegression::getB0()
{
	return this->b0;
}

double LogisticRegression::getB1()
{
	return this->b1;
}

double LogisticRegression::getB2()
{
	return this->b2;
}

double LogisticRegression::getConvergeThreshold()
{
	return this->convergeThreshold;
}

double LogisticRegression::getLearningRate()
{
	return this->learningRate;
}

double LogisticRegression::getProbabilityThreshold()
{
	return this->probabilityThreshold;
}

void LogisticRegression::predict(std::vector<double> yPoints, std::vector<double> xPoints)
{
	for (int j = 0; j < xPoints.size(); j++)
	{
		double predictedValue = 1 / (1 + exp(this->b2 * yPoints.at(j) + this->b1 * xPoints.at(j) + this->b0));
		this->predictedValues.push_back(predictedValue);
	}
}

void LogisticRegression::runAlgorithm(dataManagement & loadedData, std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	this->setTrainingData(loadedData.trainData);
	this->setTestData(loadedData.testData);
	this->setValidationData(loadedData.validationData);
	this->fit(textBoxVector);
	this->testPerformance(textBoxVector);
}

void LogisticRegression::setB0(double b0)
{
	this->b0 = b0;
}

void LogisticRegression::setB1(double b1)
{
	this->b1 = b1;
}

void LogisticRegression::setB2(double b2)
{
	this->b2 = b2;
}

void LogisticRegression::setConvergeThreshold(tgui::Slider::Ptr slider)
{
	this->convergeThreshold = slider->getValue();
}

void LogisticRegression::setLearningRate(tgui::Slider::Ptr slider)
{
	this->learningRate = slider->getValue();
}

void LogisticRegression::setProbabilityThreshold(tgui::Slider::Ptr slider)
{
	this->probabilityThreshold = slider->getValue();
}

void LogisticRegression::setTestData(std::vector<std::vector<double>> data)
{
	this->modelTestData = data;
}

void LogisticRegression::setTrainingData(std::vector<std::vector<double>> data)
{
	this->modelTrainingData = data;
}

void LogisticRegression::setValidationData(std::vector<std::vector<double>> data)
{
	this->modelValidationData = data;
}

void LogisticRegression::testPerformance(std::vector<tgui::TextBox::Ptr> textBoxVector)
{
	double performance = 0.0;
	int count = 0;
	predict(this->modelTestData[1], this->modelTestData[0]);
	for (int j = 0; j < predictedValues.size(); j++)
	{
		double prediction = 0.0;
		//probability above threshold, assign 1 as a result
		if (predictedValues.at(j) > this->probabilityThreshold)
		{
			prediction++;
		}
		std::cout << prediction << " -> " << this->modelTestData[2][j] << std::endl;
		textBoxVector.at(logRegTestResults)->addText(std::to_string(prediction) + " -> " + std::to_string(this->modelTestData[2][j]) + " \n");
		if (prediction == this->modelTestData[2][j])
		{
			count++;
		}
	}
	performance = ((double)count*100.0) / ((double)modelTestData[2].size());
	printf("Tested Performance = %.3f %%\n", performance);
	textBoxVector.at(logRegTestResults)->addText("Tested Performance = " + std::to_string(performance) +" \n");
	this->predictedValues.clear();
}